package com.tsystems.javaschool.tasks.calculator.nodes;

public class ConstantNode implements ExprNode  {

    private double value;

    public ConstantNode(double value) {
        this.value = value;
    }

    public ConstantNode(String value) {
        this.value = Double.valueOf(value);
    }

    public double getValue() {
        return value;
    }

    public int getType() {
        return ExprNode.CONSTANT_NODE;
    }
}
