package com.tsystems.javaschool.tasks.calculator.parser;


/**
 * A token produced from Tokenizer
 */

public class Token
{
    public static final int TERMINATE = 0;
    public static final int PLUSMINUS = 1;
    public static final int MULTDIV = 2;
    public static final int OPEN_BRACKET = 3;
    public static final int CLOSE_BRACKET = 4;
    public static final int NUMBER = 5;

    /**identifier **/
    public final int token;
    /**string that creates token**/
    public final String sequence;
    /**the position of the token**/
    public final int pos;

    /**
     * Constructor of a token
     * @param token - identifier
     * @param sequence - matched string
     * @param pos - position in parsed expression
     */
    public Token(int token, String sequence, int pos)
    {
        super();
        this.token = token;
        this.sequence = sequence;
        this.pos = pos;
    }
}
