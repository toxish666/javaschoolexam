package com.tsystems.javaschool.tasks.calculator.nodes;

public class AdditionNode extends Expr {

    public AdditionNode(){super();}
    public AdditionNode(ExprNode node, boolean positive) {
        super(node, positive);
    }

    public int getType() {
        return ExprNode.ADDITION_NODE;
    }

    public double getValue() {
        double sum = 0.0;
        for (Term t : terms) {
            if (t.positive)
                sum += t.expression.getValue();
            else
                sum -= t.expression.getValue();
        }
        return sum;
    }
}
