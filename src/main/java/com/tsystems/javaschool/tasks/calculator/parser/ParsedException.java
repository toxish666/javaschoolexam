package com.tsystems.javaschool.tasks.calculator.parser;

public class ParsedException extends RuntimeException {

    /** the token that caused the error */
    private Token token = null;

    public ParsedException(String message)
    {
        super(message);
    }

    public ParsedException(String message, Token token) {
        super(message);
        this.token = token;
    }

    public String getMessage() {
        String msg = super.getMessage();
        if (token != null)
            msg = msg.replace("%s", token.sequence);

        return msg;
    }
}
