package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.nodes.Expr;
import com.tsystems.javaschool.tasks.calculator.nodes.ExprNode;
import com.tsystems.javaschool.tasks.calculator.parser.ParsedException;
import com.tsystems.javaschool.tasks.calculator.parser.Parser;

public class Calculator {



    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        //construct Parser class here and pass it statement to evaluate
        Parser parser = new Parser();
        ExprNode expression;
        try{
            expression = parser.parse(statement);
        }
        catch (ParsedException e){
            System.out.println(e.getMessage());
            return null;
        }
       /* catch (EvaluatedException e){
            System.out.println(e.getMessage());
            return null;
        }*/

        Double d;
       try{
           d = expression.getValue();
       }catch (ParsedException e){
           System.out.println(e.getMessage());
           return null;
       }

       int len = d.toString().split("\\.")[1].length();
       Long a=null;
       Integer b=null;
       if(len > 4)
          a= Math.round(d);
       else if (len== 1 && d/d.intValue()==1)
           b=d.intValue();

       if(a!=null)
            return String.valueOf(a);
       else if(b!=null)
           return String.valueOf(b);
       else
           return String.valueOf(d);
    }

}

