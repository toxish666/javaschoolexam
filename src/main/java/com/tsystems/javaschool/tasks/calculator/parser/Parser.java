package com.tsystems.javaschool.tasks.calculator.parser;

import com.tsystems.javaschool.tasks.calculator.nodes.AdditionNode;
import com.tsystems.javaschool.tasks.calculator.nodes.ConstantNode;
import com.tsystems.javaschool.tasks.calculator.nodes.ExprNode;
import com.tsystems.javaschool.tasks.calculator.nodes.MultiplicationNode;

import java.util.LinkedList;

/**
 * A parser for math. expression
 */
public class Parser {

    /** parsing tokens */
    LinkedList<Token> tokens;

    /** the token to parse */
    Token token_at;

    public ExprNode parse(String expression) {
        if(expression == null || expression.length()==0)
            throw new ParsedException("Empty input string was passed");

        Tokenizer tokenizer = Tokenizer.getExpressionTokenizer();
        tokenizer.tokenize(expression);
        LinkedList<Token> tokens = tokenizer.getTokens();
        return this.parse(tokens);
    }

    public ExprNode parse(LinkedList<Token> tokens) {
        // implementing a recursive descent parser
        this.tokens = (LinkedList<Token>) tokens.clone();
        token_at = this.tokens.getFirst();

        // enterance in evaluation
        ExprNode expr = expression();

        //something goes wrong
        if (token_at.token != Token.TERMINATE)
            throw new ParsedException("Unexpected symbol %s found", token_at);

        return expr;
    }


    private void nextToken() {
        tokens.pop();
        // at the end of input we return an epsilon token
        if (tokens.isEmpty())
            token_at = new Token(Token.TERMINATE, "", -1);
        else
            token_at = tokens.getFirst();
    }

    /** handles the non-terminal expression */
    private ExprNode expression()
    {
        // expression -> signed sum
        ExprNode expr = signedTerm();
        expr = sum(expr);
        return expr;
    }

    /** handles the non-terminal sum */
    private ExprNode sum(ExprNode expr){
        // sum -> +|- term sum
        if (token_at.token == Token.PLUSMINUS){
            AdditionNode sum_a;
            if (expr.getType() == ExprNode.ADDITION_NODE)
                sum_a = (AdditionNode) expr;
            else
                sum_a = new AdditionNode(expr, true);


            boolean positive = token_at.sequence.equals("+");
            nextToken();
            ExprNode term = term();
            sum_a.add(term, positive);

            return sum(sum_a);

        }
        return expr;
    }

    /** handles the non-terminal signed_term */
    private ExprNode signedTerm(){
        // signedTerm -> +|- term
        if ( token_at.token == Token.PLUSMINUS){
            boolean positive =  token_at.sequence.equals("+");
            nextToken();
            ExprNode term = term();
            if (positive)
                return term;
            else
                return new AdditionNode(term, false);
        }

        // signed -> term
        return term();
    }


    /** handles the non-terminal term */
    private ExprNode term() {
        // term -> argument term_1
        ExprNode factor = argument();
        return term_1(factor);
    }


    /** handles the non-terminal term_1 */
    private ExprNode term_1(ExprNode expression) {
        // term_1 -> *|/ argument term_1
        if (token_at.token == Token.MULTDIV) {
            MultiplicationNode prod;
            if (expression.getType() == ExprNode.MULTIPLICATION_NODE)
                prod = (MultiplicationNode) expression;
            else
                prod = new MultiplicationNode(expression, true);

            boolean positive = token_at.sequence.equals("*");
            nextToken();
            ExprNode factor = argument();
            prod.add(factor, positive);

            return term_1(prod);
        }

        // term_1 -> TERMINATE
        return expression;
    }


    /** handles the non-terminal argument */
    private ExprNode argument() {
        // argument -> OPEN_BRACKET sum CLOSE_BRACKET
        if (token_at.token == Token.OPEN_BRACKET) {
            nextToken();
            ExprNode expr = expression();
            if (token_at.token != Token.CLOSE_BRACKET)
                throw new ParsedException("Closing brackets expected", token_at);
            nextToken();
            return expr;
        }

        // argument -> value
        return value();
    }

    /** handles the non-terminal value */
    private ExprNode value() {
        // argument -> NUMBER
        if (token_at.token == Token.NUMBER) {
            ExprNode expr = new ConstantNode(token_at.sequence);
            nextToken();
            return expr;
        }


        if (token_at.token == Token.TERMINATE)
            throw new ParsedException("Unexpected end of input");
        else
            throw new ParsedException("Unexpected symbol %s found", token_at);
    }



}
