package com.tsystems.javaschool.tasks.calculator.parser;

import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class for reading string and outputting list of tokens
 */
public class Tokenizer {
    /** information about individual token*/
    private class TokenInfo{
        /** matching regular expression */
        public final Pattern regex;
        /** id */
        public final int token;

        public TokenInfo(Pattern regex, int token)
        {
            super();
            this.regex = regex;
            this.token = token;
        }
    }

    /** a list of TokenInfo objects */
    private LinkedList<TokenInfo> tokenInfos;

    /** the list of tokens produced when tokenizing the input */
    private LinkedList<Token> tokens;

    /** a tokenizer that can handle mathematical expressions */
    private static Tokenizer expressionTokenizer = null;


    public Tokenizer() {
        super();
        tokenInfos = new LinkedList<TokenInfo>();
        tokens = new LinkedList<Token>();
    }

    public static Tokenizer getExpressionTokenizer() {
        if (expressionTokenizer == null)
            expressionTokenizer = createExpressionTokenizer();
        return expressionTokenizer;
    }

    private static Tokenizer createExpressionTokenizer() {
        Tokenizer tokenizer = new Tokenizer();

        tokenizer.add("[+-]", Token.PLUSMINUS);
        tokenizer.add("[*/]", Token.MULTDIV);
        tokenizer.add("\\(", Token.OPEN_BRACKET);
        tokenizer.add("\\)", Token.CLOSE_BRACKET);
        //integer / double
        tokenizer.add("(?:\\d+\\.?|\\.\\d)\\d*(?:[Ee][-+]?\\d+)?", Token.NUMBER);

        return tokenizer;
    }

    public void add(String regex, int token) {
        // ^ - beggining of the string
        tokenInfos.add(new TokenInfo(Pattern.compile("^(" + regex+")"), token));
    }

    public void tokenize(String str) {
        String s = str.trim();
        int totalLength = s.length();
        tokens.clear();
        while (!s.equals("")) {
            int remaining = s.length();
            boolean match = false;
            for (TokenInfo info : tokenInfos) {
                Matcher m = info.regex.matcher(s);
                if (m.find()) {
                    match = true;
                    String tok = m.group().trim();
                    s = m.replaceFirst("").trim();
                    tokens.add(new Token(info.token, tok, totalLength - remaining));
                    break;
                }
            }
            if (!match)
                throw new ParsedException("Unexpected character in input: " + s);
        }
    }


    public LinkedList<Token> getTokens()
    {
        return tokens;
    }

}
