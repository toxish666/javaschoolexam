package com.tsystems.javaschool.tasks.calculator.nodes;

import com.tsystems.javaschool.tasks.calculator.parser.ParsedException;

public class MultiplicationNode extends Expr {
    public MultiplicationNode() {}
    public MultiplicationNode(ExprNode a, boolean positive)
    {
        super(a, positive);
    }

    public int getType() {
        return ExprNode.MULTIPLICATION_NODE;
    }

    public double getValue() {
        double prod = 1.0;
        for (Term t : terms) {
            if (t.positive)
                prod *= t.expression.getValue();
            else {
                if (t.expression.getValue()==0)
                    throw new ParsedException("Zero denominator");
                prod /= t.expression.getValue();
            }
        }
        return prod;
    }
}
