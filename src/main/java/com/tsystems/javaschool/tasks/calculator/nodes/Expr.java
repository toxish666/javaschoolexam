package com.tsystems.javaschool.tasks.calculator.nodes;

import java.util.ArrayList;

public abstract class Expr implements ExprNode{

    protected ArrayList<Term> terms;

    public class Term
    {
        /** the boolean flag */
        public boolean positive;
        /** the expression node */
        public ExprNode expression;

        public Term(boolean positive, ExprNode expression)
        {
            super();
            this.positive = positive;
            this.expression = expression;
        }
    }


    public Expr(){
        this.terms = new ArrayList<Term>();
    }

    public Expr(ExprNode node, boolean positive){
        this.terms = new ArrayList<Term>();
        this.terms.add(new Term(positive,node));
    }

    public void add(ExprNode node, boolean positive) {
        this.terms.add(new Term(positive, node));
    }
}
