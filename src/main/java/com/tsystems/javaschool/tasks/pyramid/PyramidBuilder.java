package com.tsystems.javaschool.tasks.pyramid;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {

        /** Since (according to the unit-test examples) the number of inputted integers
         * is equal to the n_th partial sum of the series of natural numbers(1+2+3+4+...),
         * it seems legit to throw an exception when the number of integers is not one of the partial sums.
         *
         * Examples: 3 is OK (S = n*(n+1)/2 = 3, n = 2);
         *          6 is OK( --\\--, n = 3); 10; 15; 21; ...
         *          5 is NOT OK(there is no such n="integer" that give S = 5;
         *          256 is NOT OK(S = 253, n=22 and S = 276, n=23)
         */

        int the_number;
        try{
            the_number= Math.toIntExact(checkNumberLegit(inputNumbers.size()));
        }catch (CannotBuildPyramidException p) {
            throw p;
        }
        if(inputNumbers.contains(null)){
            throw new CannotBuildPyramidException("Numbers are set incorrect");
        }


        int[][] pyramid = new int[the_number][the_number*2 - 1];
        List<Integer> copyNumbers = new LinkedList<>(inputNumbers);
        copyNumbers.sort(Comparator.naturalOrder());

        Iterator<Integer> copyIterator = copyNumbers.iterator();


        // flag - in order to intermit row with zeroes
        for(int i=0; i<pyramid.length; i++){
            boolean flag = true;
            for(int j=0; j<pyramid[i].length; j++){

                if((j>=(the_number*2 - 1)/2 - i) && (j<=(the_number*2 - 1)/2 + i) && (flag)){
                    pyramid[i][j] = copyIterator.next();
                    flag=false;
                }
                else{
                    pyramid[i][j] = 0;
                    flag = true;
                }

            }
        }
        return pyramid;

    }

    /**
     *
     * @param len - given number of inputted integers
     * @return in case of when len is correct - return n number(n - the number of summirized natural numbers)
     *          else - throw an exception
     */
    private long checkNumberLegit(int len){
        long i  = 1;
        while(true){
            long s =  i*(i+1)/2;
            if(s < len){
                i++;
                continue;
            }
            else if(s==len){
                return i;
            }
            else
                throw new CannotBuildPyramidException("Wrong number of inputted integers");
        }
    }


}
