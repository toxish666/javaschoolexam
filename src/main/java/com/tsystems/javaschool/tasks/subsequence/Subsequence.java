package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(x==null||y==null)
            throw new IllegalArgumentException();
        Iterator yIter = y.iterator();
        Iterator xIter = x.iterator();

        while (xIter.hasNext()){
            Object xElem = xIter.next();
            if(!yIter.hasNext())
                return false;
            do{
                if(xElem.equals(yIter.next()))
                    break;
            }while (yIter.hasNext());
        }
        return true;
    }
}
